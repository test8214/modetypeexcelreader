﻿using CsvHelper.Configuration;
using CsvHelper;
using ExcelReaderTool.Model;
using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace ExcelReaderTool
{
    public partial class Form1 : Form
    {
        public BindingList<ModelType> ModelTypeData { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ModelTypeData = new BindingList<ModelType>(ReadModelTypeFile());
            dataGridView.DataSource = ModelTypeData;
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            SaveModelTypeFile(new List<ModelType>(ModelTypeData));
            MessageBox.Show("Save Done");
        }

        private List<ModelType> ReadModelTypeFile()
        {
            var readConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true
            };
            using (var reader = new StreamReader("ModelType.csv"))
            using (var csv = new CsvReader(reader, readConfiguration))
            {
                var records = csv.GetRecords<ModelType>();
                return records.ToList();
            }
        }

        private void SaveModelTypeFile(List<ModelType> datas)
        {
            var writeConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true
            };

            using (var writer = new StreamWriter("ModelType.csv"))
            using (var csv = new CsvWriter(writer, writeConfiguration))
            {
                csv.WriteRecords(datas);
            }
        }

    
    }
}
