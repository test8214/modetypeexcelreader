﻿namespace ExcelReaderTool.Model
{
    public class ModelType
    {
        public string serial_number { get; set; }

        public string model_name { get; set; }

        public string model_type { get; set;}

        public string fwver_bbox { get; set; }

        public string fwver_nb { get; set; }

        public string fwver_kernel { get; set; }

        public string hwver_bbox { get; set; }

        public string hwver_nb { get; set; }

        public string cellularmfr { get; set; }

        public string cellularmodel { get; set; }

        public string cellularfwversion { get; set; }

        public string cellulararevision { get; set; }

        public string cellularlrevision { get; set; }

        public string power_phase { get; set; }

        public string ev_connector { get; set; }

        public string certification { get; set; }
        public string rfid_mfr { get; set; }

        public string rfid_model { get; set;}

        public string rfid_fwver { get; set; }

        public string allow_offline { get; set; }

        public string exception { get; set; }

    }
}
